import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Voyage from './components/Voyage';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import List from './components/List';


const Root = () => (
    <Router>
        <Switch>
            <Route path="/:id" component={Voyage} />
            <Route path="/" component={List} />
        </Switch>
    </Router>
)

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
