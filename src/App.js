import React from 'react';
import List from './components/List';
import './App.scss';




function App() {
  return (
    <div className="App">
      <List className='list' />
    </div>
  );
}

export default App;
