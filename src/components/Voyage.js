import React , { Component } from 'react'
import data from '../data'

export const Voyage = ({match}) => console.log(match) || (
    <div>
        {data.map(element =>{
            if(element.ID == match.params.id){
                return <div className='voyageUnique'>
                    <h1>{element.titre}</h1>
                    <img src={element.imageUrl} alt="logo"/>
                    <p>{element.description}</p>
                    <p> Du {element.dateDebut} au {element.dateFin} </p>
                    <p>{element.prix} €</p>
                </div>
            }
        })}
    </div>
)
export default Voyage
