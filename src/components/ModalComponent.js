//ModalComponent.js
import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form } from 'reactstrap';
import { thisExpression } from '@babel/types';

export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  
          modal : false,
          sejour:{
            titre: '',
            imageUrl :'',
            description: '',
            theme: '',
            dateDebut:'',
            dateFin:'',
            prix:'',
            promotion:false,      
          }
        };

     this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  handleChange(event) {
    const target = event.target
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    let sejourClone = this.state.sejour
    sejourClone[name] = value
    this.setState({
      sejour: sejourClone
    });
  }

  handleSubmit(event) {
   event.preventDefault()
  
   fetch('http://localhost:8080/add.php', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
      },
      body: JSON.stringify(this.state.sejour)
    }).then(responce => console.log(responce))

    console.log(this.state)

   this.setState({
    modal: !this.state.modal
  });
     }


  render() {
    return (

        <div>
        <Button className='plus' color="success" onClick={this.toggle}>+</Button>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit} method='post'>
          <ModalHeader>Ajouter un séjour</ModalHeader>
          <ModalBody>
          <div className="row">
            <div className="form-group col-md-12">
            <label>Titre:</label>
            <input type="text" onChange={this.handleChange} className="form-control" name='titre' value={this.state.titre} />
              </div>
              </div>
            <div className="row">
             <div className="form-group col-md-12">
            <label>Url image:</label>
                <input type="text" onChange={this.handleChange} className="form-control" name='imageUrl' value={this.state.imageUrl} />
               </div>
              </div>
            <div className="row">
             <div className="form-group col-md-12">
              <label>Description:</label>
                <input type="text"  onChange={this.handleChange} className="form-control" name='description' value={this.state.description} />
               </div>
              </div>
              <div className="row">
             <div className="form-group col-md-12">
             <label>Theme:</label>
                <select value={this.state.value} onChange={this.handleChange} name='theme' value={this.state.theme}>
                  <option value="grapefruit">Grapefruit</option>
                  <option value="lime">Lime</option>
                  <option value="coconut">Coconut</option>
                  <option value="mango">Mango</option>
                </select>
               </div>
              </div>
              <div className="row">
             <div className="form-group col-md-12">
              <label>Date de début:</label>
                <input type="text"  onChange={this.handleChange} className="form-control" name='dateDebut' value={this.state.dateDebut} />
               </div>
              </div>
              <div className="row">
             <div className="form-group col-md-12">
              <label>Date de fin:</label>
                <input type="text"  onChange={this.handleChange} className="form-control" name='dateFin' value={this.state.dateFin} />
               </div>
              </div>
              <div className="row">
             <div className="form-group col-md-12">
              <label>Prix:</label>
                <input type="text" onChange={this.handleChange} className="form-control" name='prix' value={this.state.prix} />
               </div>
              </div>
              <div className="row">
             <div className="form-group col-md-12">
              <label>Promotion:</label>
                <input type="checkbox"  onChange={this.handleChange} className="form-control" name='promotion' value={this.state.promotion} />
               </div>
              </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Ajouter" color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
          </form>
        </Modal>
        </div>
      
    );
  }
}