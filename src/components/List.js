import React, { Component } from 'react';
import Voyage from './Voyage';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Button, Jumbotron, Form, Modal, Row, Container, Grid, Col } from 'react-bootstrap'
import ModalComponent from './ModalComponent';

class List extends Component {
    constructor() {
        super();
        this.state = {
            list: [],
            isLoading: false,
            selectInput: '',
            show: false
        }
        this.triprix = this.triprix.bind(this)
        this.triDate = this.triDate.bind(this)
        this.filtreTheme = this.filtreTheme.bind(this)

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.deleteSejour = this.deleteSejour.bind(this)
    }

    componentDidMount() {
        this.setState({ isLoading: true })
        fetch('http://localhost:8080/')
            .then(response => response.json())
            .then(data => {
                this.setState({ list: data, isLoading: false })  
            })
    }

    triprix() {
        let dataTrierPrix = this.state.list.sort((a, b) => a.list_prix - b.list_prix)
        this.setState({
            list: dataTrierPrix
        })
    }

    triDate() {
        let dataTrierDate = this.state.list.sort(function (a, b) {
            a = new Date(a.list_dateDebut);
            b = new Date(b.list_dateDebut);
            return a > b ? -1 : a < b ? 1 : 0;
        })

        this.setState({
            list: dataTrierDate
        })
    }

    filtreTheme(e) {

        let dataFiltrer = this.state.list.filter(element =>
            element.list_theme == e.target.value
        )

        this.setState({
            list: dataFiltrer
        })
    }

    handleClose() {
        this.setState({ show: false });
      }
    
      handleShow() {
        this.setState({ show: true });
      }

    deleteSejour(){

        fetch('http://localhost:8080/delete.php') 
        this.setState({ show: false });
    }



    render() {
        if (this.state.isLoading) {
            return <p>Loading...</p>
        }
        return (
            <div>
                <Jumbotron className='text-center'>
                    <h1>Simplon séjours</h1>
                </Jumbotron>
                <div className='text-center'>
                    <input onClick={this.triprix} type="radio" name='tri' /> Trier par prix
            <input onClick={this.triDate} type="radio" name='tri' /> Trier par date
        
        </div>

                <div>

                    <Form>

                        <Form.Group controlId="exampleForm.ControlSelect1" style={{ width: '18rem' }} className='text-center select'>
                            <Form.Label>Choisir un thème</Form.Label>
                            <Form.Control as="select" onChange={this.filtreTheme} value={this.state.selectInput} >
                                <option value="default">par default</option>
                                <option value="Exotisme">Exotisme</option>
                                <option value="Farniente">Farniente</option>
                                <option value="Culture">Culture</option>
                                <option value="Authentique">Authentique</option>
                                <option value="Aventure">Aventure</option>
                            </Form.Control>
                        </Form.Group>

                    </Form>



                    <h1>{this.state.selectInput}</h1>
                </div>
                <div className="list">

                    {this.state.list.map(element => {
                        return <div key={element.list_ID} className='card'>
                            <Card style={{ width: '18rem' }}>
                            <Card.Text className='suppr' onClick={this.handleShow}>
                                x
                            </Card.Text>
                            

                            <Modal show={this.state.show} onHide={this.handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Supprimer un séjour</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>Etes vous sur de vouloir supprimer ce séjour ? </Modal.Body>
                            <Modal.Footer>
                                <Button variant="danger" onClick={this.handleClose}>
                                Annuler
                                </Button>
                                    <Button type='submit' variant="success" onClick={this.deleteSejour}>
                                   Valider
                                    </Button>
                     
                            </Modal.Footer>
                            </Modal>
                                <Link to={element.list_ID}>  <Card.Img variant="top" src={element.list_imageUrl} /> </Link>
                                <Card.Body>
                                    <Card.Title>{element.list_titre}</Card.Title>
                                    <Card.Text>
                                        {element.list_description} <br />
                                        {element.list_prix} €
                                            </Card.Text>

                                    <Button variant="secondary">Ajouter aux favoris</Button>
                                </Card.Body>
                            </Card>
                        </div>

                    })}
                    <div>
                    <ModalComponent/>  
                    </div>

                </div>
            </div>
        );
    }

}

export default List;
